# EasyTools.SimpleBloomFilter

#### 介绍

- 引入命名空间 EasyTools.SimpleBloomFilter.Lib
- 先运行配置方法 Config()
- 调用 SetBitArray() / GetBitArray()

- Using EasyTools.SimpleBloomFilter.Lib
- Call Config()
- Use SetBitArray() / GetBitArray()

#### 安装教程
```cmd
Install-Package EasyTools.SimpleBloomFilter -Version 1.0.1
```

#### 使用说明
```csharp
using EasyTools.SimpleBloomFilter.Lib;

// step 1
BloomFilterService.Config(fpp, fieldCount);

// step 2
BloomFilterService.SetBitArray(key);
BloomFilterService.GetBitArray(key);
```
