using EasyTools.SimpleBloomFilter.Lib;
using System;
using Xunit;
using Xunit.Abstractions;

namespace LibTest
{
    public class BloomFilterServiceTest
    {
        private readonly ITestOutputHelper testOutputHelper;

        public BloomFilterServiceTest(ITestOutputHelper testOutputHelper)
        {
            this.testOutputHelper = testOutputHelper;
        }

        [Fact]
        public void ConfigTest()
        {
            BloomFilterService.Config();

            for (var i = 0; i < 10000; i++)
            {
                BloomFilterService.SetBitArray(i.ToString());
            }

            var count = 0;
            for (var i = 0; i < 10000; i++)
            {

                if (BloomFilterService.GetBitArray(i.ToString()))
                {
                    count++;
                }
            }

            testOutputHelper.WriteLine($"正确的数量为：{count}");
        }
    }
}
