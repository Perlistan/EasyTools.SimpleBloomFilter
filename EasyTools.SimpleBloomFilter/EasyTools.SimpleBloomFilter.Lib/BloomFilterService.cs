﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace EasyTools.SimpleBloomFilter.Lib
{
    public static class BloomFilterService
    {
        // BitArray 大小
        private static int BitArrayLength { get; set; }

        // 创建 Hash 的数量（用多个 Hash 匹配更精确）
        private static byte HashCount { get; set; }

        // 位数组
        private static BitArray BitArray { get; set; }



        /// <summary>
        /// 配置方法
        /// </summary>
        /// <param name="fpp">能接受的错误率</param>
        /// <param name="fieldCount">数据库某个字段的数据量</param>
        public static void Config(float fpp = 0.001f, int fieldCount = 10000)
        {
            // 设置布隆数组大小
            BitArrayLength = (int)Math.Round(-(fieldCount * Math.Log(fpp) / Math.Pow(Math.Log(2), 2)));

            // 设置合适的 bit 集合数量
            HashCount = (byte)Math.Round(BitArrayLength / fieldCount * Math.Log(2));

            BitArray = new BitArray(BitArrayLength);
        }



        /// <summary>
        /// 获取 key 的哈希集合
        /// </summary>
        /// <param name="key"></param>
        private static List<int> GetHashList(string key)
        {
            var hash = key.GetHashCode();
            var random = new Random(hash);
            var list = new List<int>();

            for (var i = 0; i < HashCount; i++)
            {
                list.Add(random.Next() % BitArrayLength);
            }

            return list;
        }



        /// <summary>
        /// 根据哈希集合添加 Bloom
        /// </summary>
        /// <param name="key"></param>
        public static void SetBitArray(string key)
        {
            var list = GetHashList(key);

            foreach (var data in list)
            {
                BitArray.Set(data, true);
            }
        }



        /// <summary>
        /// 根据哈希集合查询 Bloom
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool GetBitArray(string key)
        {
            var list = GetHashList(key);

            var count = 0;
            foreach (var data in list)
            {
                if (BitArray[data])
                {
                    count++;
                }
            }

            return count == HashCount;
        }
    }
}
